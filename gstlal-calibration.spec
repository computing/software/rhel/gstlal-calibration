%define gstreamername gstreamer1
%global __python %{__python3}

Name: gstlal-calibration
Version: 1.5.8
Release: 1.1%{?dist}
Summary: GSTLAL Calibration
License: GPLv2+
Group: LSC Software/Data Analysis

Requires: fftw >= 3
Requires: gobject-introspection >= 1.30.0
Requires: gsl
Requires: %{gstreamername} >= 1.14.1
Requires: %{gstreamername}-plugins-base >= 1.14.1
Requires: %{gstreamername}-plugins-good >= 1.14.1
Requires: lal >= 7.2.4
Requires: gstlal >= 1.10.0
Requires: gstlal-ugly >= 1.10.0
Requires: git
Requires: python3 >= 3.6
Requires: python%{python3_pkgversion}-%{gstreamername}
Requires: python%{python3_pkgversion}-gobject >= 3.22
Requires: python%{python3_pkgversion}-ligo-segments >= 1.2.0
Recommends: python%{python3_pkgversion}-confluent-kafka

%if 0%{?rhel} == 8
Requires: python%{python3_pkgversion}-numpy >= 1.7.0
Requires: python3-matplotlib
Requires: python%{python3_pkgversion}-scipy
%else
Requires: numpy >= 1.7.0
Requires: python%{python3_pkgversion}-matplotlib
Requires: scipy
%endif

BuildRequires: pkgconfig >= 0.18.0
BuildRequires: fftw-devel >= 3
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: gsl-devel
BuildRequires: %{gstreamername}-devel >= 1.14.1
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.14.1
BuildRequires: lal-devel >= 7.2.4
BuildRequires: gstlal-devel >= 1.10.0
BuildRequires: python3-devel >= 3.6

# account for package name differences between rhel7/8
%if 0%{?rhel} == 8
BuildRequires: pygobject3-devel >= 3.22
BuildRequires: python%{python3_pkgversion}-numpy >= 1.7.0
%else
BuildRequires: python36-gobject-devel >= 3.22
BuildRequires: numpy >= 1.7.0
%endif

Conflicts: gstlal-ugly < 0.6.0
Source: https://software.igwn.org/lscsoft/source/gstlal-calibration-%{version}.tar.gz
URL: https://git.ligo.org/Calibration/gstlal-calibration
Packager: Madeline Wade <madeline.wade@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package contains the plugins and shared libraries required to run the
gstlal calibration software.


%package devel
Summary: Requirements for compiling gstlal-calibration based plugins
Group: LSC Software/Data Analysis
Requires: %{name} = %{version}-%{release}
Requires: fftw-devel >= 3
Requires: gsl-devel
Requires: %{gstreamername}-devel >= 1.14.1
Requires: %{gstreamername}-plugins-base-devel >= 1.14.1
Requires: lal-devel >= 7.2.4
Requires: gstlal-devel >= 1.10.0
Requires: python3-devel >= 3.6
%description devel
This package contains the files needed for building gstlal-calibration based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure PYTHON=python3
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlalcalibration/*
%{_libdir}/gstreamer-*/lib*.so
%{_prefix}/%{_lib}/python*/site-packages/gstlalcalibration
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/gstreamer-*/lib*.a
%{_libdir}/pkgconfig/*
%{_includedir}/*


%changelog
* Thu Jul 18 2024 Madeline Wade <madeline.wade@ligo.org> - 1.5.7-1
- Fix oversubtraction
- Modified output channels that report information about configs
- Fix for a line subtraction gating bug
- Fix for a line subtraction failure
- Print config, filters, and version info on startup
- Significantly reduced pipeline startup time
- No more warning messages, etc., at pipeline startup

* Mon Jun 10 2024 Aaron Viets <aaron.viets@ligo.org> - 1.5.6-1
- Print config, filters, and version info on startup
- New option --version

* Thu Jun 06 2024 Aaron Viets <aaron.viets@ligo.org> - 1.5.5-1
- Mitigation of oversubtraction of spectral lines
- Improved channels with filters, config, and version info
- Fix for line subtraction gating bug caused by typecasting error
- Improved handling of missing input data for line subtraction
- A few basic unit tests

* Wed Nov 29 2023 Madeline Wade <madeline.wade@ligo.org> - 1.5.4-1
- Replace nans with zeros in the subtracted NOLINES signal
- Debian packaging fix

* Wed Nov 22 2023 Aaron Viets <aaron.viets@ligo.org> - 1.5.3-1
- Ability to report configuration and filters info in the frames
- Require timestamp syncing of filter updates for reproducibility
- New EPICS naming scheme with variable names rather than numbers
- Do not require the NonSENS subtraction channel to be present
- Package python modules in gstlalcalibration, not in gstlal
- Replace nans in NOLINES channel with zeros during lock-loss
- Option to adjust maximum queue length in config file
- Option to use a threshold or channel to start filter clock
- Gate noise subtraction TFs with amplitude channels
- Filters ok bit fix

* Mon Apr 17 2023 Madeline Wade <madeline.wade@ligo.org> - 1.5.2-1
- Bug fix for line subtraction

* Thu Mar 23 2023 Madeline Wade <madeline.wade@ligo.org> - 1.5.1-1
- Compatability fixes for pyGobject

* Wed Mar 22 2023 Madeline Wade <madeline.wade@ligo.org> - 1.5.0-1
- Updates to calibration state vector
- Ability to apply nonsens cleaning channel
- Compatability fixes for pyGobject
- Fix for integer string conversion error

* Wed Nov 16 2022 Madeline Wade <madeline.wade@ligo.org> - 1.4.0-1
- Additional of gstlal_correct_strain pipeline
- Functionality for uncertainty estimation
- Processing functions for swept sine measurements

